#
# ~/.bash_profile
#

MANPATH="${MANPATH:-/usr/share/man}"
INFOPATH="${INFOPATH:-/usr/share/info}"

export PATH="$HOME/usr/bin${PATH:+:}$PATH"
export CPATH="$HOME/usr/include${CPATH:+:}$CPATH"
export LIBRARY_PATH="$HOME/usr/lib${LIBRARY_PATH:+:}$LIBRARY_PATH"
export LD_LIBRARY_PATH="$HOME/usr/lib${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH"
export MANPATH="$HOME/usr/share/man${MANPATH:+:}$MANPATH"
export INFOPATH="$HOME/usr/share/info${INFOPATH:+:}$INFOPATH"

# Activate GNU Guix profiles
GUIX_EXTRA_PROFILES="${XDG_CONFIG_HOME:-$HOME/.config}/guix/profiles"
for profile in ; do
  GUIX_PROFILE="$GUIX_EXTRA_PROFILES/$profile/$profile"
  if [ -f "$GUIX_EXTRA_PROFILES/$profile/$profile/etc/profile" ]; then
    . "$GUIX_PROFILE/etc/profile"
  fi
done
# Might as well set to default if it's needed
GUIX_PROFILE="$HOME/.guix-profile"

[[ -f ~/.bashrc ]] && . ~/.bashrc
