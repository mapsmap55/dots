#
# ~/.bashrc
#

export SHELL

if [[ $- != *i* ]]; then
  [[ -n "$SSH_CLIENT" ]] && source /etc/profile
  return
fi

if [ -f /etc/bashrc ]; then
  source /etc/bashrc
fi

shopt -s checkjobs checkwinsize globstar histappend

PS1='\[\e[m\][\[\e[1;33m\]\u@\h\[\e[m\] \[\e[1;37m\]\W\[\e[m\]]\$ '
if [ -n "$GUIX_ENVIRONMENT" ]; then
  PS1="${PS1:0:9}"'\[\e[1;32m\](env)\[\e[m\] '"${PS1:9}"
fi

HISTCONTROL=ignoreboth
HISTSIZE=768
HISTFILESIZE=1536

export EDITOR='emacs -nw'
export VISUAL="$EDITOR"

alias ls='ls --color=auto'
alias temacs='emacs -nw'
