(define terminal
  '("acl" "cmus" "curl" "ed" "ffmpeg" "libcap" "links" "openssh" "qemu" "recutils" "rsync" "screen" "stow"))

(define development
  '("git"
    "autoconf" "automake" "libtool" "make" "gcc-toolchain" "gdb" "valgrind" "valgrind:doc" "strace"
    "rust" "rust:cargo" "rust:tools"
    "guile" "mit-scheme"
    "clisp" "sbcl" "sicp"
    "gforth"
    "python"))

(define docs
  '("man-pages" "man-pages-posix" "man-db" "info-reader"))

(define emacs
  '("emacs" "emacs-guix"
    "emacs-magit"
    "emacs-markdown-mode" "emacs-markdown-preview-mode"
    "emacs-rust-mode"
    "emacs-paredit" "emacs-geiser" "emacs-geiser-guile" "emacs-slime"))

(define xorg
  '("xautolock" "xsetroot" "xbindkeys" "xrandr"))

(define gui
  '("pavucontrol" "font-liberation"
    "icecat" "firefox"
    "scrot" "mupdf" "vlc" "keepassxc" "anki"))

(define games
  '("dwarf-fortress"))

(define wine
  '("wine64" "wine" "winetricks" "dxvk"))

(specifications->manifest
 (append terminal development docs emacs xorg gui games wine))
