(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/"))

(package-initialize)

(defun install-my-packages (&optional refresh)
  "Install the various packages I use. If REFRESH is not NIL, call
PACKAGE-REFRESH-CONTENTS first."
  (interactive "n")
  (when refresh
    (package-refresh-contents))
  (dolist (name '(slime geiser paredit markdown-mode))
    (package-install name)))

(tool-bar-mode -1)
(menu-bar-mode -1)

(require 'slime-autoloads)
(setq inferior-lisp-program "sbcl")

(add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook #'enable-paredit-mode)
(add-hook 'lisp-mode-hook #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)

(setq c-default-style "bsd"
      c-basic-offset 2)
(c-set-offset 'case-label '+)
(c-set-offset 'access-label '/)
(setq-default indent-tabs-mode nil)

(defun delete-trailing-whitespace-notext ()
  "Runs delete-trailing-whitespace, unless in text-mode"
  (interactive)
  (unless (derived-mode-p 'text-mode)
    (delete-trailing-whitespace)))
(add-hook 'before-save-hook 'delete-trailing-whitespace-notext)

(let ((recutils-path "~/.emacs.d/recutils/"))
 (when (file-exists-p recutils-path)
   (add-to-list 'load-path recutils-path)
   (require 'rec-mode)
   (add-to-list 'auto-mode-alist '("\\.rec\\'" . rec-mode))))

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 `(custom-enabled-themes ',(if (display-graphic-p) '(tango-dark) '()))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(company smart-tabs-mode markdown-mode paredit slime geiser)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Liberation Mono" :foundry "1ASC" :slant normal :weight normal :height 98 :width normal)))))
